from flask import Flask,render_template,request,url_for
import os
import glob
from werkzeug.utils import secure_filename
import pdf_operation
app = Flask(__name__)
UPLOAD_FOLDER = 'static/Pdf_File'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
path = os.getcwd()

@app.route('/uploadfile', methods=['GET', 'POST'])
def uploadfile():
    # os.chdir(r"E:/OCR_PROJECT/venv")
    print("upload file")
    if request.method == 'POST':
            print("post method")

            if 'chooseFile' not in request.files:
                        # print("file not found")
                        return "file not found"
            else:
                        # print("file found")
                        file = request.files['chooseFile']
                        filename = secure_filename(file.filename)
                        files = glob.glob('static/Pdf_File/*')
                        for f in files:
                            print(f)
                            filepath = f
                            if os.path.exists(filepath):
                                # os.remove(filepath)
                                print("file deleted")
                        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                        rs=pdf_operation.results(filename)
                        # rs = []
                        # print(rs[2])
                        return render_template("validator.html", Header=rs[0], ext_val=rs[1], c_list=rs[2])

    else:
        print("get method")
        return render_template("index.html", number=0, filename='d', preview=0)
@app.route('/result', methods=['GET', 'POST'])
def result():
    print("result api")
    if request.method == 'POST':
            print("post method")

            return render_template("result.html")
    else:


        print("get method")

        return render_template("result.html")
if __name__=='__main__':
    app.run(host='0.0.0.0', port=5000,debug=True)