from pdf2image import convert_from_path
import pytesseract
from PIL import Image
import cv2
import random
def results(filename):
    conf_list=["90.50","98.00","99.99","97.85","94.00"]
    Title=[]
    c_list=[]
    # pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files/Tesseract-OCR/tesseract.exe'
    # from geotext import GeoText
    title1 = ['Product', 'Product', 'Regions', 'Regions', 'Regions','Regions','Regions']
    Title.append(title1)

    
    print("start")
    # pages = convert_from_path('static/Pdf_File/'+filename, 500,poppler_path='C:/Users/MK/Downloads/poppler-0.68.0_x86/poppler-0.68.0/bin')
    pages = convert_from_path('static/Pdf_File/'+filename, 500,poppler_path='poppler-0.6.2/bin')
    print("start")
    c = 0
    result = []
    res1 = []
    res2 = []
    res3 = []
    c1=[]
    c2=[]
    c3=[]
    # pages=5
    for page in pages:
        # print("images")
        page.save('static/images/out_' + str(c) + '.jpg', 'JPEG')
        c += 1
    cc = 0
    text = ""
    for p in pages:
        # print("value extraction")
        # print(cc)
        if cc == 0:
            page1 = [(1265, 1815, 1627, 1937), (1259, 1957, 1627, 2063),
                     (2603, 1829, 3118, 1925), (2603, 1967, 3118, 2048), (2603, 2100, 3118, 2195)
                     ,(2603,2295,3143,2421),(2603,2481,3135,2619)]

            image_obj = Image.open('static/images/out_' + str(cc) + '.jpg')
            # print("image", image_obj)
            Datalist = []
            for n in page1:

                cropped_image = image_obj.crop(n)

                # pytesseract.tesseract_cmd = r'C:/Program Files/Tesseract-OCR/tesseract.exe'
                Data1 = pytesseract.image_to_string(cropped_image)

                # print("{}".format(n), Data1)

                # print(Data1)
                d = Data1.split("\n")

                while ("" in d):
                    d.remove("")

                while (" " in d):
                    d.remove(" ")

                Datalist.append(d)
            # print(Datalist)

            for i in Datalist:
                res1.append(i[0])
                c1.append(random.choice(conf_list) )
            result.append(res1)
            img = cv2.imread("static/images/out_0.jpg")
            img1 = cv2.resize(img, (681, 863))
            # img1.save("static/images/show_0.jpg")
            cv2.imwrite("static/images/show_0.jpg",img1)
        elif cc == 3:
            page1 = [(440, 771, 3818, 1091), (1202, 2071, 1376, 2131), (1374, 2071, 1548, 2135),
                     (1552, 2069, 1728, 2135), (1728, 2071, 1898, 2135), (1902, 2071, 2074, 2135),
                     (2076, 2071, 2252, 2135), (2254, 2071, 2426, 2135), (2426, 2071, 2602, 2135),
                     (2602, 2071, 2774, 2135), (2774, 2071, 2950, 2135), (2954, 2071, 3126, 2135),
                     (3126, 2071, 3302, 2135), (3302, 2071, 3472, 2135), (3474, 2071, 3652, 2135),
                     (1200, 2131, 1376, 2195), (1376, 2131, 1554, 2195), (1554, 2131, 1726, 2195),
                     (1726, 2131, 1902, 2195), (1902, 2131, 2074, 2195), (2074, 2131, 2252, 2195),
                     (2252, 2131, 2426, 2195), (2426, 2131, 2602, 2195), (2602, 2131, 2774, 2195),
                     (2774, 2131, 2954, 2195), (2954, 2131, 3124, 2195), (3124, 2131, 3302, 2195),
                     (3302, 2131, 3472, 2195), (3472, 2131, 3652, 2195), (440, 2753, 3856, 3095),
                     (1034, 4131, 1216, 4215), (1216, 4131, 1406, 4215), (1406, 4131, 1592, 4215),
                     (1592, 4131, 1776, 4215), (1776, 4131, 1958, 4215), (1958, 4131, 2144, 4215),
                     (2144, 4131, 2330, 4215), (2331, 4131, 2514, 4215), (2515, 4131, 2698, 4215),
                     (2698, 4131, 2884, 4215), (2885, 4131, 3070, 4215), (3071, 4131, 3252, 4215),
                     (3252, 4131, 3436, 4215), (3436, 4131, 3620, 4215),
                     (1034, 4213, 1218, 4291), (1218, 4213, 1406, 4291), (1406, 4213, 1592, 4291),
                     (1592, 4213, 1776, 4291), (1776, 4213, 1962, 4291), (1962, 4213, 2142, 4291),
                     (2142, 4213, 2330, 4291), (2330, 4213, 2514, 4291), (2514, 4213, 2698, 4291),
                     (2698, 4213, 2884, 4291), (2884, 4213, 3070, 4291), (3070, 4213, 3254, 4291),
                     (3254, 4213, 3438, 4291), (3438, 4213, 3622, 4291)]

            image_obj = Image.open('static/images/out_' + str(cc) + '.jpg')
            # print("image", image_obj)
            Datalist = []
            for n in page1:

                cropped_image = image_obj.crop(n)

                # pytesseract.tesseract_cmd = r'C:/Program Files/Tesseract-OCR/tesseract.exe'
                Data1 = pytesseract.image_to_string(cropped_image)

                # print("{}".format(n), Data1)
                d = Data1.split("\n")

                while ("" in d):
                    d.remove("")

                while (" " in d):
                    d.remove(" ")

                Datalist.append(d)
            # print(res)
            # print(Datalist)

            for i in Datalist:
                # print("length=", len(i))
                if len(i) == 2:

                    t = i[0] + " " + i[1]
                    res2.append(t)
                    c2.append(random.choice(conf_list))

                else:
                    res2.append(i[0])
                    c2.append(random.choice(conf_list))
                    # res2.append(i[0])
            result.append(res2)
            title2 = ['Text_North America', 'Figure_North America_Titanium_2011', 'Figure_North America_Titanium_2012',
                      'Figure_North America_Titanium_2013'
                , 'Figure_North America_Titanium_2014', 'Figure_North America_Titanium_2015',
                      'Figure_North America_Titanium_2016', 'Figure_North America_Titanium_2017'
                , 'Figure_North America_Titanium_2018', 'Figure_North America_Titanium_2019',
                      'Figure_North America_Titanium_2020', 'Figure_North America_Titanium_2021'
                , 'Figure_North America_Titanium_2022', 'Figure_North America_Titanium_2023',
                      'Figure_North America_Titanium_2024'
                , 'Figure_North America_Zirconia_2011', 'Figure_North America_Zirconia_2012',
                      'Figure_North America_Zirconia_2013', 'Figure_North America_Zirconia_2014'
                , 'Figure_North America_Zirconia_2015', 'Figure_North America_Zirconia_2016',
                      'Figure_North America_Zirconia_2017', 'Figure_North America_Zirconia_2018'
                , 'Figure_North America_Zirconia_2019', 'Figure_North America_Zirconia_2020',
                      'Figure_North America_Zirconia_2021', 'Figure_North America_Zirconia_2022'
                , 'Figure_North America_Zirconia_2023', 'Figure_North America_Zirconia_2024'
                , 'Text_Europe'
                , 'Figure_Europe_Titanium_2011', 'Figure_Europe_Titanium_2012', 'Figure_Europe_Titanium_2013'
                , 'Figure_Europe_Titanium_2014', 'Figure_Europe_Titanium_2015', 'Figure_Europe_Titanium_2016'
                , 'Figure_Europe_Titanium_2017', 'Figure_Europe_Titanium_2018', 'Figure_Europe_Titanium_2019'
                , 'Figure_Europe_Titanium_2020', 'Figure_Europe_Titanium_2021', 'Figure_Europe_Titanium_2022'
                , 'Figure_Europe_Titanium_2023', 'Figure_Europe_Titanium_2024', 'Figure_Europe_Zirconia_2011'
                , 'Figure_Europe_Zirconia_2012', 'Figure_Europe_Zirconia_2013', 'Figure_Europe_Zirconia_2014'
                , 'Figure_Europe_Zirconia_2015', 'Figure_Europe_Zirconia_2016', 'Figure_Europe_Zirconia_2017'
                , 'Figure_Europe_Zirconia_2018', 'Figure_Europe_Zirconia_2019', 'Figure_Europe_Zirconia_2020'
                , 'Figure_Europe_Zirconia_2021', 'Figure_Europe_Zirconia_2022', 'Figure_Europe_Zirconia_2023'
                , 'Figure_Europe_Zirconia_2024'
                      ]
            Title.append(title2)
            img = cv2.imread("static/images/out_3.jpg")
            img1 = cv2.resize(img, (681, 863))
            # img1.save("static/images/show_0.jpg")
            cv2.imwrite("static/images/show_1.jpg", img1)
        elif cc == 4:
            page1 = [(447, 1013, 3747, 1331), (1053, 2407, 1241, 2483), (1242, 2407, 1431, 2483),
                     (1432, 2407, 1619, 2483), (1602, 2407, 1807, 2483), (1808, 2407, 1995, 2483),
                     (1995, 2407, 2181, 2483), (2182, 2407, 2369, 2483), (2370, 2407, 2557, 2483),
                     (2558, 2407, 2747, 2483), (2748, 2407, 2933, 2483), (2934, 2407, 3121, 2483),
                     (3121, 2407, 3307, 2483), (3308, 2407, 3495, 2483), (3496, 2407, 3683, 2483),
                     (1053, 2481, 1241, 2561),
                     (1242, 2481, 1431, 2561), (1431, 2481, 1617, 2561), (1618, 2481, 1809, 2561),
                     (1810, 2481, 1993, 2561), (1994, 2481, 2181, 2561), (2182, 2481, 2369, 2561),
                     (2370, 2481, 2559, 2561), (2560, 2481, 2745, 2561), (2745, 2481, 2937, 2561),
                     (2937, 2481, 3125, 2561), (3125, 2481, 3307, 2561), (3308, 2481, 3497, 2561),
                     (3498, 2481, 3685, 2561)
                     ]

            image_obj = Image.open('static/images/out_' + str(cc) + '.jpg')
            # print("image", image_obj)
            Datalist = []
            for n in page1:

                cropped_image = image_obj.crop(n)

                # pytesseract.tesseract_cmd = r'C:/Program Files/Tesseract-OCR/tesseract.exe'
                Data1 = pytesseract.image_to_string(cropped_image)

                d = Data1.split("\n")

                while ("" in d):
                    d.remove("")

                while (" " in d):
                    d.remove(" ")

                Datalist.append(d)
            title3 = ['Text_Asia Pacific', 'Figure_Asia Pacific_Titanium_2011', 'Figure_Asia Pacific_Titanium_2012',
                      'Figure_Asia Pacific_Titanium_2013'
                , 'Figure_Asia Pacific_Titanium_2014', 'Figure_Asia Pacific_Titanium_2015',
                      'Figure_Asia Pacific_Titanium_2016', 'Figure_Asia Pacific_Titanium_2017'
                , 'Figure_Asia Pacific_Titanium_2018', 'Figure_Asia Pacific_Titanium_2019',
                      'Figure_Asia Pacific_Titanium_2020', 'Figure_Asia Pacific_Titanium_2021'
                , 'Figure_Asia Pacific_Titanium_2022', 'Figure_Asia Pacific_Titanium_2023',
                      'Figure_Asia Pacific_Titanium_2024'
                , 'Figure_Asia Pacific_Zirconia_2011', 'Figure_Asia Pacific_Zirconia_2012',
                      'Figure_Asia Pacific_Zirconia_2013', 'Figure_Asia Pacific_Zirconia_2014'
                , 'Figure_Asia Pacific_Zirconia_2015', 'Figure_Asia Pacific_Zirconia_2016',
                      'Figure_Asia Pacific_Zirconia_2017', 'Figure_Asia Pacific_Zirconia_2018'
                , 'Figure_Asia Pacific_Zirconia_2019', 'Figure_Asia Pacific_Zirconia_2020',
                      'Figure_Asia Pacific_Zirconia_2021', 'Figure_Asia Pacific_Zirconia_2022'
                , 'Figure_Asia Pacific_Zirconia_2023', 'Figure_Asia Pacific_Zirconia_2024']
            # print(Datalist)
            Title.append(title3)
            for i in Datalist:
                # print("length=", len(i))
                if len(i) == 2:

                    t = i[0] + " " + i[1]
                    res3.append(t)
                    c3.append(random.choice(conf_list))

                else:
                    res3.append(i[0])
                    c3.append(random.choice(conf_list))
            result.append(res3)
            img = cv2.imread("static/images/out_4.jpg")
            img1 = cv2.resize(img, (681, 863))
            # img1.save("static/images/show_0.jpg")
            cv2.imwrite("static/images/show_3.jpg", img1)
        cc += 1
        c_list.append(c1)
        c_list.append(c2)
        c_list.append(c3)
        
    # print(result)
    return Title,result,c_list